// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const config = require('config');
const moment = require('moment');

const logger = require('../logger');

// --------------------------------------------------------
// Runner -------------------------------------------------
// --------------------------------------------------------
module.exports = connection => {
	setInterval(() => {
		let minimumTime = moment().utc().subtract(config.keepData, 'ms');

		connection.query(`DELETE FROM playercount WHERE timestamp < '${minimumTime.format('YYYY-MM-DD HH:mm:ss')}'`, err => {
			if (err) {
				logger.error('An exception occurred whilst trying to delete timestamps:', err.toString());
			}
		});
	}, config.updateInterval);
};
