// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const config = require('config');
const https = require('https');

const logger = require('../logger');

// --------------------------------------------------------
// Runner -------------------------------------------------
// --------------------------------------------------------
fetchPlayercount = callback => {
	try {
		https.get(config.endpoint, res => {
			let data = '';
			res.setEncoding('utf8');
			res.on('data', receivedData => {
				data += receivedData;
			});
			res.on('end', () => {
				try {
					let registeredGames = [];
					config.games.keys.forEach(game => {
						keys = Object.keys(game);
						if (keys.length === 1) {
							registeredGames.push(keys[0]);
						}
					});

					let playercount = JSON.parse(data);

					let filteredPlayercount = Object
							.keys(playercount)
							.filter(key => registeredGames.indexOf(key) > -1)
							.filter(key => !isNaN(playercount[key]))
							.reduce((res, key) => (res[key] = playercount[key], res), {});

					callback(filteredPlayercount);
				} catch (err) {
					logger.error('An exception occurred whilst trying to parse data from endpoint:', err.toString());
					callback(false);
				}
			});
		});
	} catch (err) {
		logger.error('An exception occurred whilst trying to contact endpoint:', err.toString());
		callback(false);
	}
};

module.exports = connection => {
	setInterval(() => {
		fetchPlayercount(res => {
			if (res !== false) {
				let cols = [];
				let vals = [];
				for (let key in res) {
					cols.push(`\`playercount\`.\`${key}\``);
					vals.push(res[key]);
				}

				connection.query(`INSERT INTO playercount (${cols.join(', ')}) VALUES (${vals.join(', ')});`, err => {
					if (err) {
						logger.error('An exception occurred whilst inserting timestamp:', err.toString());
					}
				});
			}
		});
	}, config.updateInterval);
};
