// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const express = require('express');

// --------------------------------------------------------
// API setup ----------------------------------------------
// --------------------------------------------------------
module.exports = connection => {
	let api = express.Router();

	api.get('/config', require('./get/config'));
	api.get('/playercount', require('./get/playercount')(connection));

	return api;
};
