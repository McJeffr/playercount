// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const config = require('config');
const moment = require('moment');

const logger = require('../../../logger');

// --------------------------------------------------------
// Method -------------------------------------------------
// --------------------------------------------------------
module.exports = connection => {
	return (req, res) => {
		if (typeof req.query.top !== 'undefined' && typeof req.query.current !== 'undefined') {
			res.status(400);
			res.json({ status: 400, message: 'Cannot combine \`current\` and \`top\` query parameters in a single request' });
			return;
		}

		if (typeof req.query.type !== 'undefined' && types.indexOf(req.query.type) < 0) {
			res.status(400);
			res.json({ status: 400, message: 'Invalid type provided. Please check the API documentation for valid types.' });
			return;
		}

		let registeredGames = [];
		config.games.keys.forEach(game => {
			keys = Object.keys(game);
			if (keys.length === 1) {
				registeredGames.push(keys[0]);
			}
		});

		let games = null;
		if (req.query.games) {
			games = [];
			req.query.games.split(',').forEach(game => {
				if (registeredGames.indexOf(game) > -1) {
					games.push(game);
				}
			});
		}

		let after = null;
		if (req.query.after) {
			let timestamp = parseInt(req.query.after);
			if (isNaN(timestamp)) {
				res.status(400);
				res.json({ status: 400, message: 'Invalid timestamp provided in query parameter \'after\'' });
				return;
			}

			after = moment.utc(timestamp * 1000).format('YYYY-MM-DD HH:mm:ss');
		}

		let query;
		if (typeof req.query.top !== 'undefined') {
			if (games && games.length > 0) {
				let maxGames = games.map(game => {
					return `MAX(\`${game}\`) AS \`${game}\``;
				});
				query = `SELECT ${maxGames.join(', ')} FROM \`playercount\``;
			} else {
				let maxRegisteredGames = registeredGames.map(registeredGame => {
					return `MAX(\`${registeredGame}\`) AS \`${registeredGame}\``;
				});
				query = `SELECT ${maxRegisteredGames.join(', ')} FROM \`playercount\``;
			}
		} else {
			let columns;
			if (games && games.length > 0) {
				columns = games.map(game => {
					return `\`${game}\``;
				});
			} else {
				columns = registeredGames.map(registeredGame => {
					return `\`${registeredGame}\``;
				});
			}
			query = `SELECT \`timestamp\`, ${columns.join(', ')} 
					FROM \`playercount\` 
					${after ? `WHERE \`playercount\`.\`timestamp\` > '${after}'` : ''} 
					${typeof req.query.current !== 'undefined' ? 'ORDER BY \`playercount\`.\`timestamp\` DESC LIMIT 1' : ''};`;
		}

		connection.query(query, (err, results) => {
			if (err) {
				logger.error('An exception occurred whilst trying to fetch timestamps:', err.toString());
				res.status(500);
				res.json({ status: 500, message: 'Could not fetch from database' });
				return;
			}

			if (typeof req.query.top !== 'undefined') {
				if (results.length === 0) {
					res.json({});
					return;
				}

				let maxPlayercount = {};
				for (game in results[0]) {
					maxPlayercount[game] = results[0][game];
				}

				res.json(maxPlayercount);
			} else {
				if (typeof req.query.current === 'undefined' || req.query.current === false) {
					let scale = Math.ceil(results.length / config.maxDataPoints);
					let type = req.query.type ? req.query.type : 'avg';
					results = reduceResults(results, scale, type);
				}

				res.json(results.map(timestamp => {
					let games = {};

					for (game in timestamp) {
						if (game !== 'timestamp') {
							games[game] = timestamp[game];
						}
					}

					return {
						date: new Date(timestamp.timestamp),
						data: games
					}
				}));
			}
		});
	};
};

// --------------------------------------------------------
// Helper Methods -----------------------------------------
// --------------------------------------------------------
const types = [
	'avg',
	'min',
	'max'
];

function avgTimestamp(fragment) {
	let avg = {};

	// Sum up the playercounts of each timestamp
	fragment.forEach(timestamp => {
		for (game in timestamp) {
			if (game !== 'timestamp') {
				avg[game] = (avg[game] ? avg[game] : 0) + timestamp[game];
			}
		}
	});

	// Calculate the average of the summed up timestamp
	for (game in avg) {
		avg[game] = Math.round(avg[game] / fragment.length);
	}

	// Assign the time of the first timestamp to the average timestamp
	avg.timestamp = fragment[0].timestamp;

	return avg;
};

function minTimestamp(fragment) {
	let min = {};

	// Get the minimum of each game from the fragment
	fragment.forEach(timestamp => {
		for (game in timestamp) {
			if (game !== 'timestamp') {
				if (!min[game] || min[game] > timestamp[game]) {
					min[game] = timestamp[game];
				}
			}
		}
	});

	// Assign the time of the first timestamp to the minimum timestamp
	min.timestamp = fragment[0].timestamp;

	return min;
}

function maxTimestamp(fragment) {
	let max = {};

	// Get the minimum of each game from the fragment
	fragment.forEach(timestamp => {
		for (game in timestamp) {
			if (game !== 'timestamp') {
				if (!max[game] || max[game] < timestamp[game]) {
					max[game] = timestamp[game];
				}
			}
		}
	});

	// Assign the time of the first timestamp to the minimum timestamp
	max.timestamp = fragment[0].timestamp;

	return max;
}

function reduceResults(data, scale, type) {
	let result = [];

	for (let i = 0; i < data.length - scale; i += scale) {
		switch(type) {
			case 'min':
				result.push(minTimestamp(data.slice(i, i + scale)));
				break;
			case 'max':
				result.push(maxTimestamp(data.slice(i, i + scale)));
				break;
			default:
				result.push(avgTimestamp(data.slice(i, i + scale)));
		}
	}

	return result;
};