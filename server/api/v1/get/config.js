// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const config = require('config');

// --------------------------------------------------------
// Method  ------------------------------------------------
// --------------------------------------------------------
module.exports = (req, res) => {
	res.json({
		endpoint: config.endpoint,
		updateInterval: config.updateInterval,
		keepData: config.keepData,
		maxDataPoints: config.maxDataPoints,
		games: config.games
	});
};
