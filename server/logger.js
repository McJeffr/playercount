// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const fs      = require('fs');
const winston = require('winston');

const logDirectory = 'logs';

// --------------------------------------------------------
// Logger -------------------------------------------------
// --------------------------------------------------------

// If there is not yet a directory to store logs, create one as winston won't do this
if (!fs.existsSync(logDirectory) ) {
	fs.mkdirSync(logDirectory);
}

// Get the winston config object and add transports for each event
let config = winston.config;
let logger = new (winston.Logger)({
	transports: [
		new (winston.transports.File)({
			name: 'error-file',
			filename: logDirectory + '/error.log',
			level: 'error',
			handleExceptions: true
		}),
		new (winston.transports.File)({
			name: 'combined-file',
			filename: logDirectory + '/combined.log',
			handleExceptions: true
		})
	]
});

logger.exitOnError = false;

// If the service isn't running in production, add a console logger
if (process.env.NODE_ENV != 'production') {
	logger.add(winston.transports.Console, {
			timestamp: function() {
				return new Date().toISOString();
			},
			formatter: function(options) {
				return options.timestamp() + ' ' +
					config.colorize(options.level, options.level.toUpperCase()) + ' ' +
					(options.message ? options.message : '') +
					(options.meta && Object.keys(options.meta).length ? '\n\t'+ JSON.stringify(options.meta) : '' );
			},
			handleExceptions: true
		});
}

// --------------------------------------------------------
// Export -------------------------------------------------
// --------------------------------------------------------
module.exports = logger;