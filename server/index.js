// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const async = require('async');
const config = require('config');
const bodyParser = require('body-parser');
const express = require('express');
const mysql = require('mysql');

const api = require('./api');
const logger = require('./logger');

// --------------------------------------------------------
// Server startup process ---------------------------------
// --------------------------------------------------------
async.waterfall([
	// Create database connection
	callback => {
		callback(null, mysql.createConnection(config.database.options));
	},
	// Establish database connection
	(connection, callback) => {
		connection.connect(err => {
			callback(err, connection);
		});
	},
	// Set the timezone of the session
	(connection, callback) => {
		connection.query(`SET time_zone = 'utc';`, err => {
			callback(err, connection);
		});
	},
	// Check if the database is set up correctly and set it up if needed
	(connection, callback) => {
		async.waterfall([
			// Check if the schema exists and create it if needed
			innerCallback => {
				connection.query(`CREATE SCHEMA IF NOT EXISTS \`${config.database.schema}\`;`, err => {
					innerCallback(err);
				});
			},
			// Use the schema
			innerCallback => {
				connection.query(`USE \`${config.database.schema}\`;`, err => {
					innerCallback(err);
				});
			},
			// Check if the table exists and create it if needed
			innerCallback => {
				connection.query(`CREATE TABLE IF NOT EXISTS \`${config.database.schema}\`.\`playercount\` (\`timestamp\` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (\`timestamp\`));`, err => {
					innerCallback(err);
				});
			},
			// Check if the correct columns in the table exist
			innerCallback => {
				connection.query(`SHOW COLUMNS FROM \`playercount\`;`, (err, res) => {
					if (err) {
						innerCallback(err);
						return;
					}

					columns = res.reduce((acc, column) => {
						if (column.Field !== 'timestamp') {
							acc.push(column.Field);
						}
						return acc;
					}, []);

					missingColumns = config.games.keys.reduce((acc, game) => {
						if (columns.indexOf(Object.keys(game)[0]) < 0) {
							acc.push(Object.keys(game)[0]);
						}
						return acc;
					}, []);

					innerCallback(null, missingColumns);
				});
			},
			(missingColumns, innerCallback) => {
				if (missingColumns.length === 0) {
					innerCallback(null);
					return;
				}

				let queue = async.queue((game, queueCallback) => {
					connection.query(`ALTER TABLE \`playercount\` ADD \`${game}\` INT(11) NOT NULL DEFAULT 0`);
					queueCallback();
				}, 1);

				// Queue callback
				queue.drain = () => {
					innerCallback(null);
				};

				missingColumns.forEach(game => {
					queue.push(game, (err) => {
						if (err) {
							innerCallback(err);
						}
					});
				});
			}
		], err => {
			callback(err, connection);
		});
	},
	// Setup the app
	(connection, callback) => {
		setupApp(connection, callback);
	},
	// Startup the app
	(app, callback) => {
		startApp(app, callback);
	}
], err => {
	if (err) {
		logger.error('An exception occurred whilst trying to connect to database:', err.toString());
		return;
	}
	logger.info('Service startup completed');
});

// --------------------------------------------------------
// App setup ----------------------------------------------
// --------------------------------------------------------
setupApp = (connection, callback) => {
	let app = express();
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json());

	// --------------------------------------------------------
	// API setup ----------------------------------------------
	// --------------------------------------------------------
	app.use('/api/v1', api(connection).v1);

	// --------------------------------------------------------
	// Runner setup -------------------------------------------
	// --------------------------------------------------------
	require('./runner/fetcher')(connection);
	if (config.keepData > 0) {
		require('./runner/purger')(connection);
	}

	// --------------------------------------------------------
	// UI setup -----------------------------------------------
	// --------------------------------------------------------
	app.use('/', express.static('./app/dist'));

	// --------------------------------------------------------
	// Development setup --------------------------------------
	// --------------------------------------------------------
	if (process.env.NODE_ENV === 'development') {
		const webpack = require('webpack');
		const webpackConfig = require('../app/webpack.development.js');
		const compiler = webpack(webpackConfig);

		app.use(require("webpack-dev-middleware")(compiler, {
			noInfo: true, publicPath: webpackConfig.output.publicPath
		}));

		app.use(require("webpack-hot-middleware")(compiler));
	}

	callback(null, app);
}

// --------------------------------------------------------
// App startup --------------------------------------------
// --------------------------------------------------------
startApp = (app, callback) => {
	// Fetch port
	const port = process.env.PORT || config.port;

	// Listen app on port
	app.listen(port);

	// Call the callback
	callback(null);
}
