// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import axios from 'axios';
import subSeconds from 'date-fns/sub_seconds';
import PropTypes from 'prop-types';
import { Component } from 'react';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import Row from 'react-bootstrap/lib/Row';

import { Card } from '../reusable/Card.jsx';
import { GameMultiSelect } from '../reusable/GameMultiSelect.jsx';
import { PlayercountGraph } from '../reusable/PlayercountGraph.jsx';
import { Section } from '../reusable/Section.jsx';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
const dataInterval = [
	{
		3600: {
			title: '1 hour',
			format: 'hh:mm A'
		}
	},
	{
		86400: {
			title: '1 day',
			format: 'ddd, h A'
		}
	},
	{
		604800: {
			title: '1 week',
			format: 'MMM Do'
		}
	},
	{
		2592000: {
			title: '1 month',
			format: 'MMM Do'
		}
	}
];

const types = [
	'avg',
	'min',
	'max'
];

export class GraphsSection extends Component {

	/**
	 * Constructor for a new GraphsSection component.
	 * @constructor
	 * @param {Object} props - The React props object.
	 */
	constructor(props) {
		super(props);

		// Bind the functions to 'this'
		this.fetchPlayercount = this.fetchPlayercount.bind(this);
		this.onIntervalSelect = this.onIntervalSelect.bind(this);
		this.onTypeSelect = this.onTypeSelect.bind(this);
		this.onGameSelect = this.onGameSelect.bind(this);

		this.state = {
			interval: dataInterval[0],
			type: types[0],
			games: props.config.games.defaultDisplay,
			data: null
		};
	}

	/**
	 * This React function gets fired when the component is about to mount.
	 */
	componentWillMount() {
		this.fetchPlayercount(dataInterval[0], types[0], this.state.games);

		this.intervalId = setInterval(() => {
			this.fetchPlayercount(dataInterval[0], types[0], this.state.games);
		}, this.props.config.updateInterval);
	}

	/**
	 * This React function gets fired when the component is about to unmount.
	 */
	componentWillUnmount() {
		clearInterval(this.intervalId);
	}

	/**
	 * This method indicates whether or not a component should update.
	 * @param  {Object} newProps - The new React props object.
	 * @param  {Object} newState - The new state object.
	 * @return {Boolean} True if the component should update, false otherwise.
	 */
	shouldComponentUpdate(newProps, newState) {
		return newProps !== this.props || newState !== this.state;
	}

	/**
	 * This method will launch an API call to fetch the playercounts from the API based on the filtered properties.
	 * @param {Object} interval - The interval at which the data needs to be fetched at.
	 * @param {String} type - The type of data to fetch, according to the 'types' array.
	 * @param {Array} games - An array containing the games of which data needs to be fetched.
	 */
	fetchPlayercount(interval, type, games) {
		let after = Math.floor(subSeconds(new Date(), Object.keys(interval)[0]) / 1000);

		axios.get(`/api/v1/playercount?after=${after}&type=${type}${games ? `&games=${games.join(',')}` : ''}`)
			.then(response => {
				this.setState({ data: response.data });
			})
			.catch(error => {
				console.error(error);
			});
	}

	/**
	 * This event handler responds to a change in the data interval.
	 * @param {String} key - The interval that has been selected.
	 */
	onIntervalSelect(key) {
		let interval;
		dataInterval.forEach(i => {
			if (Object.keys(i)[0] === key) {
				interval = i;
			}
		});

		// If the interval hasn't changed, do not update it
		if (interval === this.state.interval) {
			return;
		}

		// Clear any previous interval
		clearInterval(this.intervalId);

		// Update the state with the new interval and clear the old data
		this.setState({ interval, data: null });

		// Fetch the playercount once so we don't need to wait for the interval
		this.fetchPlayercount(interval, this.state.type, this.state.games);

		// Set an interval to fetch the playercount
		this.intervalId = setInterval(() => {
			this.fetchPlayercount(interval, this.state.type, this.state.games);
		}, this.props.config.updateInterval);
	}

	/**
	 * This event handler responds to a change in the data type.
	 * @param {String} key - The data type that has been selected.
	 */
	onTypeSelect(key) {
		// If the type hasn't changed, do not update it
		if (key === this.state.type) {
			return;
		}

		// Clear any previous interval
		clearInterval(this.intervalId);

		// Update the state with the new type and clear the old data
		this.setState({ type: key, data: null });

		// Fetch the playercount once so we don't need to wait for the interval
		this.fetchPlayercount(this.state.interval, key, this.state.games);

		// Set an interval to fetch the playercount
		this.intervalId = setInterval(() => {
			this.fetchPlayercount(this.state.interval, key, this.state.games);
		}, this.props.config.updateInterval);
	}

	/**
	 * This event handler responds to a change in the selected games.
	 * @param {Array} games - An array of games that are selected to be displayed.
	 */
	onGameSelect(games) {
		// Clear any previous interval
		clearInterval(this.intervalId);

		// Update the state with the new set of games and clear the old data
		this.setState({ games, data: null });

		// Fetch the playercount once so we don't need to wait for the interval
		this.fetchPlayercount(this.state.interval, this.state.type, games);

		// Set an interval to fetch the playercount
		this.intervalId = setInterval(() => {
			this.fetchPlayercount(this.state.interval, this.state.type, games);
		}, this.props.config.updateInterval);
	}

	/**
	 * The React render function that renders this component.
	 * @return {Object} A React element containing this component.
	 */
	render() {
		let { config } = this.props;
		let { games } = this.state;

		// Compile the graphs of gamemodes
		let playercountGraphs = [];
		let totalKey = config.games.total;
		let totalTitle;
		config.games.keys.forEach(game => {
			let keys = Object.keys(game);

			if (keys.length === 1) {
				if (keys[0] === totalKey) {
					totalTitle = game[keys[0]];
				} else {
					if (games.indexOf(keys[0]) > -1) {
						playercountGraphs.push(
							<Card col={6} title={game[keys[0]]}>
								<PlayercountGraph data={this.state.data} game={keys[0]} interval={this.state.interval} />
							</Card>
						);
					}
				}
			}
		});

		// Add the graphs of gamemodes into rows
		let rows = [];
		for (let i = 0; i < playercountGraphs.length; i += 2) {
			rows.push(
				<Row key={i}>
					{playercountGraphs[i]}
					{playercountGraphs[i + 1] ? playercountGraphs[i + 1] : null}
				</Row>
			);
		}

		return (
			<Section id="graphs" title="Graphs">
				<Row>
					<Card col={12} title="Filters">
						<DropdownButton className="dropdown" id={"dropdownInterval"} bsStyle={"primary"} title={"Display: " + this.state.interval[Object.keys(this.state.interval)].title}>
							{dataInterval.map(interval => (
									<MenuItem
											key={Object.keys(interval)[0]}
											eventKey={Object.keys(interval)[0]}
											onSelect={this.onIntervalSelect}
											active={Object.keys(this.state.interval)[0] === Object.keys(interval)[0]}>
										{interval[Object.keys(interval)[0]].title}
									</MenuItem>
								))}
						</DropdownButton>
						<DropdownButton className="dropdown" id={"dropdownType"} bsStyle={"primary"} title={"Type: " + this.state.type}>
							{types.map(type => (
									<MenuItem
											key={type}
											eventKey={type}
											onSelect={this.onTypeSelect}
											active={this.state.type === type}>
										{type}
									</MenuItem>
								))}
						</DropdownButton>
						<GameMultiSelect games={config.games.keys} selected={games} onChange={this.onGameSelect} />
					</Card>
				</Row>
				{games.indexOf(totalKey) > -1 ? (
					<Row>
						<Card col={12} title={totalTitle}>
							<PlayercountGraph data={this.state.data} game={totalKey} interval={this.state.interval} />
						</Card>
					</Row>
				) : null}
				{rows}
			</Section>
		);
	}
}

// --------------------------------------------------------
// PropTypes ----------------------------------------------
// --------------------------------------------------------
GraphsSection.propTypes = {
	config: PropTypes.object.isRequired
}
