// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import axios from 'axios';
import PropTypes from 'prop-types';
import { Component } from 'react';
import Row from 'react-bootstrap/lib/Row';
import { RadialChart, Hint } from 'react-vis';

import { Card } from '../reusable/Card.jsx';
import { DistributionRadial } from '../reusable/DistributionRadial.jsx';
import { Section } from '../reusable/Section.jsx';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
export class LiveSection extends Component {

	/**
	 * Constructor for a new LiveSection component.
	 * @constructor
	 * @param {Object} props - The React props object.
	 */
	constructor(props) {
		super(props);

		// Bind the functions to 'this'
		this.fetchCurrentPlayercount = this.fetchCurrentPlayercount.bind(this);

		this.state = { data: null };
	}

	/**
	 * This React function gets fired when the component is about to mount.
	 */
	componentWillMount() {
		this.fetchCurrentPlayercount();

		this.intervalId = setInterval(() => {
			this.fetchCurrentPlayercount();
		}, this.props.config.updateInterval);
	}

	/**
	 * This React function gets fired when the component is about to unmount.
	 */
	componentWillUnmount() {
		clearInterval(this.intervalId);
	}

	/**
	 * This method indicates whether or not a component should update.
	 * @param  {Object} newProps - The new React props object.
	 * @param  {Object} newState - The new state object.
	 * @return {Boolean} True if the component should update, false otherwise.
	 */
	shouldComponentUpdate(newProps, newState) {
		return newProps !== this.props || newState !== this.state;
	}

	/**
	 * This method launches an API call to fetch the current playercount and update the state with it.
	 */
	fetchCurrentPlayercount() {
		axios.get('/api/v1/playercount?current')
			.then(response => {
				this.setState({ data: response.data });
			})
			.catch(error => {
				console.error(error);
			});
	}

	/**
	 * The React render function that renders this component.
	 * @return {Object} A React element containing this component.
	 */
	render() {
		return (
			<Section id="live" title="Live">
				<Row>
					<Card col={12} title="Player Distribution">
						<DistributionRadial
								config={this.props.config}
								data={this.state.data}
								width={250}
								height={250}
								radius={90}
								innerRadius={45} />
					</Card>
				</Row>
			</Section>
		);
	}
}

// --------------------------------------------------------
// PropTypes ----------------------------------------------
// --------------------------------------------------------
LiveSection.propTypes = {
	config: PropTypes.object.isRequired
}
