// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import axios from 'axios';
import { Component } from 'react';
import Row from 'react-bootstrap/lib/Row';

import { Card } from '../reusable/Card.jsx';
import { Section } from '../reusable/Section.jsx';
import { Spinner } from '../reusable/Spinner.jsx';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
export class MojangStatusSection extends Component {

	/**
	 * Constructor for a new MojangStatusSection component.
	 * @constructor
	 * @param {Object} props - The React props object.
	 */
	constructor(props) {
		super(props);

		this.fetchMojangStatus = this.fetchMojangStatus.bind(this);

		this.state = { mojangStatus: null }
	}

	/**
	 * This React function gets fired when the component is about to mount.
	 */
	componentWillMount() {
		this.fetchMojangStatus();

		setInterval(() => {
			this.fetchMojangStatus();
		}, 15000);
	}

	/**
	 * This React function gets fired when the component is about to unmount.
	 */
	componentWillUnmount() {
		clearInterval(this.intervalId);
	}

	/**
	 * This method indicates whether or not a component should update.
	 * @param  {Object} newProps - The new React props object.
	 * @param  {Object} newState - The new state object.
	 * @return {Boolean} True if the component should update, false otherwise.
	 */
	shouldComponentUpdate(newProps, newState) {
		return newProps !== this.props || newState !== this.state;
	}

	/**
	 * This method will fetch the status of Mojang's services and update the state with it.
	 */
	fetchMojangStatus() {
		axios.get('https://status.mojang.com/check')
			.then(response => {
				this.setState({ mojangStatus: response.data });
			})
			.catch(error => {
				console.error(error);
			});
	}

	/**
	 * The React render function that renders this component.
	 * @return {Object} A React element containing this component.
	 */
	render() {
		// If there is not yet any data to be displayed, show a loading spinner
		if (!this.state.mojangStatus) {
			return (
				<Section id="mojang-status" title="Mojang Status">
					<Row>
						<Card col={12} title="Loading">
							<Spinner size="6x" />
						</Card>
					</Row>
				</Section>
			);
		}

		// Parse all services into cards that can be rendered
		let services = [];
		this.state.mojangStatus.forEach(service => {
			let serviceTitle = Object.keys(service)[0];
			services.push(
				<Card className="mojangService" col={4} title={serviceTitle}>
					{service[serviceTitle] === 'green' ? <h1 className="mojangStatusGreen">Normal</h1> : null}
					{service[serviceTitle] === 'yellow' ? <h1 className="mojangStatusYellow">Slow</h1> : null}
					{service[serviceTitle] === 'red' ? <h1 className="mojangStatusRed">Down</h1> : null}
				</Card>
			);
		});

		// Add all cards into rows so that they are displayed
		let serviceRows = [];
		for (let i = 0; i < services.length; i += 3) {
			serviceRows.push(
				<Row key={i}>
					{services[i] ? services[i] : null}
					{services[i + 1] ? services[i + 1] : null}
					{services[i + 2] ? services[i + 2] : null}
				</Row>
			);
		}

		return (
			<Section id="mojang-status" title="Mojang Status">
				{serviceRows}
			</Section>
		);
	}
}
