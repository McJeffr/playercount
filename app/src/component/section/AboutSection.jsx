// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import { Section } from '../reusable/Section.jsx';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
export const AboutSection = () => {
	return (
		<Section id="about" title="About">
			<p>
				This web-app contains live statistics pulled from a public interface of Shotbow. This data is then processed to present it in a series of graphs and charts. Next to this, the app also displays the current status of Shotbow and Mojang, useful for checking downtimes.
			</p>
			<p>
				The web-app has been created by <a href="https://twitter.com/McJeffr" target="_blank">McJeffr</a> with as primary goal to discover the different technologies used in its creation. The app consists of two parts: a front-end and a back-end. The back-end contains a small API which stores data pulled from Shotbow's public interface. The back-end also serves the front-end of the app, which is created with React 15. The entire app runs on an Express server in NodeJS, written in JavaScript mixed with ES6 elements. Webpack is used to compile and bundle all source files so that these can be served to the front-end.
			</p>
		</Section>
	);
}
