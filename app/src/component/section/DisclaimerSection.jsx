// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import { Section } from '../reusable/Section.jsx';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
export const DisclaimerSection = () => {
	return (
		<Section id="disclaimer" title="Disclaimer">
			<p>
				This web-app has been created by <a href="https://twitter.com/McJeffr" target="_blank">McJeffr</a> and is not an official service of Shotbow. The primary goal of this app was to discover the different technologies that were used in the creation process. The data displayed in this app has been polled from a public interface provided by Shotbow. The validity of this data cannot be guaranteed nor can the uptime of the app be guaranteed.
			</p>
		</Section>
	);
}
