// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import PropTypes from 'prop-types';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
export const Nav = ({ brandImgSrc, brandName, children }) => (
	<nav id="navigation">
		<div className="brand">
			{brandImgSrc ? <img src={brandImgSrc} /> : null}
			{brandName ? <span>{brandName}</span> : null}
		</div>
		<ul>
			{children}
		</ul>
	</nav>
);

// --------------------------------------------------------
// PropTypes ----------------------------------------------
// --------------------------------------------------------
Nav.propTypes = {
	brandImgSrc: PropTypes.string,
	brandName: PropTypes.string
}
