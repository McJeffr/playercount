// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import PropTypes from 'prop-types';
import { Component } from 'react';
import Select from 'react-select';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
export class GameMultiSelect extends Component {

	/**
	 * Constructor for a new GameMultiSelect component.
	 * @constructor
	 * @param {Object} props - The React props object.
	 */
	constructor(props) {
		super(props);

		this.handleSelectChange = this.handleSelectChange.bind(this);
	}

	/**
	 * This event handler responds to a change in the selected values.
	 * @param {String} value - A string containing all values that are selected, seperated by commas.
	 */
	handleSelectChange(value) {
		let { onChange } = this.props;
		if (onChange) {
			let games = value.split(',');
			onChange(games);
		}
	}

	/**
	 * The React render function that renders this component.
	 * @return {Object} A React element containing this component.
	 */
	render() {
		let { games, selected } = this.props;

		// Create an array containing all options and one containing all selected values.
		let options = [];
		let value = [];
		games.forEach(game => {
			options.push({ value: Object.keys(game)[0], label: game[Object.keys(game)[0]] });
			if (selected.indexOf(Object.keys(game)[0]) > -1) {
				value.push({ value: Object.keys(game)[0], label: game[Object.keys(game)[0]] });
			}
		});

		return (
			<Select
				simpleValue={true}
				multi={true}
				onChange={this.handleSelectChange}
				options={options}
				placeholder="Select game(s) to be displayed"
				value={value} />
		);
	}
}

// --------------------------------------------------------
// PropTypes ----------------------------------------------
// --------------------------------------------------------
GameMultiSelect.propTypes = {
	games: PropTypes.array.isRequired,
	selected: PropTypes.array,
	onChange: PropTypes.func
}
