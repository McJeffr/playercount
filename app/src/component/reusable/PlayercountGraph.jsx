// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import format from 'date-fns/format';
import isAfter from 'date-fns/is_after';
import subSeconds from 'date-fns/sub_seconds';
import PropTypes from 'prop-types';
import { Component } from 'react';
import { FlexibleWidthXYPlot, LineSeries, VerticalGridLines, HorizontalGridLines, XAxis, YAxis, Crosshair } from 'react-vis';

import { Spinner } from './Spinner.jsx';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
const axisStyle = {
	line: {
		stroke: '#232323'
	},
	ticks: {
		stroke: '#232323'
	},
	text: {
		stroke: 'none',
		fill: '#8b8b8b',
		fontWeight: 600
	},
	title: {
		stroke: 'none',
		fill: '#8b8b8b',
		fontWeight: 600
	}
}

const gridStyle = {
	stroke: '#232323',
	strokeDasharray: '3,3'
}

const crosshairStyle = {
	line: {
		background: '#f7941d'
	}
}

export class PlayercountGraph extends Component {

	/**
	 * Constructor for a new PlayercountGraph component.
	 * @constructor
	 * @param {Object} props - The React props object.
	 */
	constructor(props) {
		super(props);

		// Bind the functions to 'this'
		this.onNearestX = this.onNearestX.bind(this);
		this.onMouseLeave = this.onMouseLeave.bind(this);
		this.processData = this.processData.bind(this);

		// Set initial state
		this.parsedData = [];
		this.state = { crosshair: [] };
	}

	/**
	 * This method indicates whether or not a component should update.
	 * @param  {Object} newProps - The new React props object.
	 * @param  {Object} newState - The new state object.
	 * @return {Boolean} True if the component should update, false otherwise.
	 */
	shouldComponentUpdate(newProps, newState) {
		return newProps !== this.props || newState !== this.state;
	}

	/**
	 * This event handler responds to the mouse hovering over a line chart.
	 * @param {Object} value - The nearest data point.
	 * @param {Object} info - An object containing three properties: innerX, index and event.
	 */
	onNearestX(value, {index}) {
		this.setState({ crosshair: [this.parsedData[index]] });
	}

	/**
	 * This event handler responds to the mouse leaving a line chart.
	 */
	onMouseLeave() {
		this.setState({ crosshair: [] });
	}

	/**
	 * This function processes data from the API to data that works for react-vis charts.
	 * @param  {Array} data - An array containing the data from the API.
	 * @param  {String} game - A string containing the name of the game that needs to be displayed.
	 * @param  {Object} interval - An object containing information about the interval that is displayed.
	 * @return {Array} An array containing data parsed to work for react-vis charts.
	 */
	processData(data, game, interval) {
		const prev = subSeconds(new Date(), Object.keys(interval)[0]);

		let parsedData = [];
		data.forEach(timestamp => {
			if (typeof timestamp.data[game] !== 'undefined' && isAfter(timestamp.date, prev)) {
				parsedData.push({ x: Math.floor(new Date(timestamp.date) / 1000), y: timestamp.data[game] })
			}
		});

		return parsedData;
	}

	/**
	 * The React render function that renders this component.
	 * @return {Object} A React element containing this component.
	 */
	render() {
		let { data, game, height = 300, color = "#e64a19" } = this.props;

		// Check if there is any data to be displayed
		if (!data) {
			return <Spinner size="4x" />;
		}

		// Parse the data to the react-vis format and check if it contains any data points
		this.parsedData = this.processData(data, game, this.props.interval);
		if (this.parsedData.length === 0) {
			return (
				<p>No data to display</p>
			);
		}

		// Calculate the current and top playercount of the parsed data
		let current = this.parsedData[0];
		let top = this.parsedData[0];
		this.parsedData.forEach(dataPoint => {
			if (dataPoint.x > current.x) {
				current = dataPoint;
			}
			if (dataPoint.y > top.y) {
				top = dataPoint;
			}
		});

		// Fetch various display properties needed for rendering the chart
		const now = new Date();
		const prev = subSeconds(now, Object.keys(this.props.interval)[0]);
		let yDomainMax = this.parsedData.reduce((acc, val) => val.y > acc ? val.y : acc, 0);

		return (
			<div>
				<FlexibleWidthXYPlot
						margin={{bottom: 60}}
						onMouseLeave={this.onMouseLeave}
						height={height}
						stroke={color}
						xDomain={[Math.floor(prev.getTime() / 1000), Math.floor(now.getTime() / 1000)]}
						yDomain={[0, yDomainMax + 5]}
						xType="time">
					<HorizontalGridLines style={gridStyle} />
					<VerticalGridLines style={gridStyle} />
					<LineSeries onNearestX={this.onNearestX} data={this.parsedData} />
					<XAxis
							title="Time"
							style={axisStyle}
							tickFormat={v => format(v * 1000, this.props.interval[Object.keys(this.props.interval)].format)}
							tickLabelAngle={-45} />
					<YAxis
							title="Players"
							style={axisStyle} />
					{this.state.crosshair.length > 0 ? 
						<Crosshair style={crosshairStyle} values={this.state.crosshair}>
							<div className="rv-crosshair__inner__content">
								<div>
									<div className="rv-crosshair__title">
										<span>{format(this.state.crosshair[0].x * 1000, 'ddd, MMM D, YYYY, hh:mm A')}</span>
									</div>
									<div className="rv-crosshair__item">
										<span>Players: {this.state.crosshair[0].y}</span>
									</div>
								</div>
							</div>
						</Crosshair>
					: null}
				</FlexibleWidthXYPlot>
				<span>Current: {current.y} | Top: {top.y}</span>
			</div>
		);
	}
}

// --------------------------------------------------------
// PropTypes ----------------------------------------------
// --------------------------------------------------------
PlayercountGraph.propTypes = {
	data: PropTypes.array,
	game: PropTypes.string.isRequired,
	height: PropTypes.number,
	color: PropTypes.string
}
