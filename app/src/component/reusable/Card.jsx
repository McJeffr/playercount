// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import PropTypes from 'prop-types';
import Col from 'react-bootstrap/lib/Col';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
export const Card = ({ className, col, title, children }) => (
	<Col md={col}>
		<Col className={`card ${className}`} md={12}>
			{title ? <div className="title">{title}</div> : null}
			<div className="content">{children}</div>
		</Col>
	</Col>
);

// --------------------------------------------------------
// PropTypes ----------------------------------------------
// --------------------------------------------------------
Card.propTypes = {
	className: PropTypes.string,
	col: PropTypes.number.isRequired,
	title: PropTypes.string
}
