// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faSpinner from '@fortawesome/fontawesome-free-solid/faSpinner'
import PropTypes from 'prop-types';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
export const Spinner = ({ color = '#8b8b8b', size = '1x' }) => (
	<div className="spinner">
		<FontAwesomeIcon
			icon={faSpinner}
			style={{color}}
			size={size}
			spin />
	</div>
);

// --------------------------------------------------------
// PropTypes ----------------------------------------------
// --------------------------------------------------------
Spinner.propTypes = {
	size: PropTypes.string,
	color: PropTypes.string
}
