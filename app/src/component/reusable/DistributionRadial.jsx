// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import isAfter from 'date-fns/is_after';
import PropTypes from 'prop-types';
import { Component } from 'react';
import Row from 'react-bootstrap/lib/Row';
import { RadialChart, Hint } from 'react-vis';

import { Card } from '../reusable/Card.jsx';
import { Section } from '../reusable/Section.jsx';
import { Spinner } from '../reusable/Spinner.jsx';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
export class DistributionRadial extends Component {

	/**
	 * Constructor for a new DistributionRadial component.
	 * @constructor
	 * @param {Object} props - The React props object.
	 */
	constructor(props) {
		super(props);

		this.onMouseEnter = this.onMouseEnter.bind(this);
		this.onMouseLeave = this.onMouseLeave.bind(this);

		this.state = { hint: false };
	}

	/**
	 * This method indicates whether or not a component should update.
	 * @param  {Object} newProps - The new React props object.
	 * @param  {Object} newState - The new state object.
	 * @return {Boolean} True if the component should update, false otherwise.
	 */
	shouldComponentUpdate(newProps, newState) {
		return newProps !== this.props || newState !== this.state;
	}

	/**
	 * This event handler responds to the mouse entering / hovering over a game in the radial chart.
	 * @param {Object} game - The game object in the radial chart of which a hint popup is being displayed.
	 */
	onMouseEnter(game) {
		this.setState({ hint: game });
	}

	/**
	 * This event handler responds to the mouse leaving / no longer hovering over a game in the radial chart.
	 */
	onMouseLeave() {
		this.setState({ hint: false });
	}

	/**
	 * The React render function that renders this component.
	 * @return {Object} A React element containing this component.
	 */
	render() {
		let { config, data, width, height, radius, innerRadius } = this.props;

		// Check if there is any data to be displayed
		if (!data) {
			return <Spinner size="4x" />;
		}

		// Check if the data is valid to prevent crashes
		if (data.length === 0) {
			return (
				<p>No data to display</p>
			);
		}

		// Fetch all gametitles in one object
		let gameTitles = config.games.keys.reduce((acc, game) => {
			return {...acc, ...game};
		}, {});

		// Sort the current timestamp based on players in a game
		let current = [];
		let total = 0;
		for (let game in data[0].data) {
			if (game === config.games.total) {
				total = data[0].data[game];
				continue;
			}
			if (data[0].data[game] === 0) {
				continue;
			}

			current.push({ [game]: data[0].data[game]});
		}
		current.sort((a, b) => {
			return b[Object.keys(b)[0]] - a[Object.keys(a)[0]];
		});

		// Transform the data into a format acceptable by react-vis
		let processedData = [];
		let color = 1;
		current.forEach(game => {
			let gameKey = Object.keys(game)[0];
			processedData.push({
				angle: game[gameKey],
				players: game[gameKey],
				percentage: Math.round(game[gameKey] / total * 1000) / 10,
				game: gameKey,
				color,
				label: gameTitles[gameKey] });
			color++;
		});

		return (
			<RadialChart
					className="distributionradial"
					colorType="linear"
					colorDomain={[0, 4, 5, 8, 9, 12]}
					colorRange={['#f7941d', '#e64a19', '#6495ed', '#003399', '#bf94e4', '#4b0082']}
					radius={radius}
					innerRadius={innerRadius}
					data={processedData}
					width={width}
					height={height}
					onValueMouseOver={this.onMouseEnter}
					onSeriesMouseOut={this.onMouseLeave}>
				{this.state.hint ?
					<Hint value={this.state.hint}>
						<div className="rv-crosshair__inner__content">
							<div>
								<div className="rv-crosshair__title">
									{gameTitles[this.state.hint.game]} {this.state.hint.percentage}%
								</div>
								<div className="rv-crosshair__item">
									Players: {this.state.hint.players}
								</div>
							</div>
						</div>
					</Hint>
				: null}
			</RadialChart>
		);
	}
}

// --------------------------------------------------------
// PropTypes ----------------------------------------------
// --------------------------------------------------------
DistributionRadial.propTypes = {
	config: PropTypes.object.isRequired,
	data: PropTypes.array,
	width: PropTypes.number.isRequired,
	height: PropTypes.number.isRequired,
	radius: PropTypes.number.isRequired,
	innerRadius: PropTypes.number.isRequired
}
