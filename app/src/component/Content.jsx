// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import axios from 'axios';
import { Component } from 'react';
import Row from 'react-bootstrap/lib/Row';

import { Card } from './reusable/Card.jsx';
import { Spinner } from './reusable/Spinner.jsx';
import { AboutSection } from './section/AboutSection.jsx';
import { DisclaimerSection } from './section/DisclaimerSection.jsx';
import { GraphsSection } from './section/GraphsSection.jsx';
import { LiveSection } from './section/LiveSection.jsx';
import { MojangStatusSection } from './section/MojangStatusSection.jsx';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
export class Content extends Component {

	/**
	 * Constructor for a new Content component.
	 * @constructor
	 * @param {Object} props - The React props object.
	 */
	constructor(props) {
		super(props);

		// Initialize default state
		this.state = { config: null, data: null };
	}

	/**
	 * This React function gets fired when the component is about to mount.
	 */
	componentWillMount() {
		// Fetch the configuration object
		axios.get('/api/v1/config')
			.then(response => {
				this.setState({ config: response.data });
			})
			.catch(error => {
				console.error(error);
			});
	}

	/**
	 * This React function gets fired when the component is about to unmount.
	 */
	componentWillUnmount() {
		clearInterval(this.intervalId);
	}

	/**
	 * This method indicates whether or not a component should update.
	 * @param  {Object} newProps - The new React props object.
	 * @param  {Object} newState - The new state object.
	 * @return {Boolean} True if the component should update, false otherwise.
	 */
	shouldComponentUpdate(newProps, newState) {
		return newState !== this.state;
	}

	/**
	 * The React render function that renders this component.
	 * @return {Object} A React element containing this component.
	 */
	render() {
		if (!this.state.config) {
			return (
				<div id="content" className="container">
					<Row>
						<Card col={12} title="Loading">
							<Spinner size="6x" />
						</Card>
					</Row>
				</div>
			);
		}

		return (
			<div id="content">
				<div className="container-fluid">
					<AboutSection />
					<DisclaimerSection />
					<GraphsSection config={this.state.config} />
					<LiveSection config={this.state.config} data={this.state.data} />
					<MojangStatusSection />
				</div>
			</div>
		);
	}
}
