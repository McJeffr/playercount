// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import { Component } from 'react';
import { hot } from 'react-hot-loader';

import { Content } from './Content.jsx';
import { Nav } from './reusable/Nav.jsx';
import { NavItem } from './reusable/NavItem.jsx';
import logo from '../img/logo.png';

// --------------------------------------------------------
// Component ----------------------------------------------
// --------------------------------------------------------
class App extends Component {

	/**
	 * Constructor for a new App component.
	 * @constructor
	 * @param {Object} props - The React props object.
	 */
	constructor(props) {
		super(props);
	}

	/**
	 * The React render function that renders this component.
	 * @return {Object} A React element containing this component.
	 */
	render() {
		return (
			<div id="wrapper">
				<Nav brandName="Status" brandImgSrc={logo}>
					<NavItem href="#about" name="About" />
					<NavItem href="#disclaimer" name="Disclaimer" />
					<NavItem href="#graphs" name="Graphs" />
					<NavItem href="#live" name="Live" />
					<NavItem href="#mojang-status" name="Mojang Status" />
				</Nav>
				<Content />
			</div>
		);
	}
}

export default hot(module)(App);
