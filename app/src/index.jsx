// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
import React from 'react';
import { render } from 'react-dom';

import './style/style.css';
import './style/react-vis.css';
import './style/react-select.css';
import App from './component/App.jsx';

// --------------------------------------------------------
// React Render Function ----------------------------------
// --------------------------------------------------------
window.React = React;

render(
	<App />,
	document.getElementById('react-container')
);
