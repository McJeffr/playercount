// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const webpack = require('webpack');
const merge = require('webpack-merge');

const defaultConfig = require('./webpack.default.js');

// --------------------------------------------------------
// Webpack configuration ----------------------------------
// --------------------------------------------------------
module.exports = merge.smartStrategy({ 'module.rules': 'replace' })(defaultConfig, {
	entry: ['./app/src/index.jsx', 'webpack-hot-middleware/client'],
	mode: 'development',
	devtool: 'source-map',
	plugins: [
		new webpack.HotModuleReplacementPlugin()
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
				query: {
					presets: ['latest', 'stage-0', 'react']
				}
			},
			{
				test: /\.css$/,
				use: [
					{
						loader: 'style-loader',
						options: {
							sourceMap: true
						}
					},
					{
						loader: 'css-loader',
						options: {
							sourceMap: true
						}
					}
				]
			},
			{
				test: /\.(png|jpg|gif)$/,
				use: ['file-loader']
			}
		]
	}
});
