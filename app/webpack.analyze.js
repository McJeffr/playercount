// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const webpack = require('webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const merge = require('webpack-merge');

const productionConfig = require('./webpack.production.js');

// --------------------------------------------------------
// Webpack configuration ----------------------------------
// --------------------------------------------------------
module.exports = merge(productionConfig, {
	plugins: [
		new BundleAnalyzerPlugin()
	]
});
