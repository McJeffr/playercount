// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const path = require('path');
const webpack = require('webpack');

// --------------------------------------------------------
// Webpack configuration ----------------------------------
// --------------------------------------------------------
module.exports = {
	entry: './app/src/index.jsx',
	output: {
		path: path.join(__dirname, 'dist/assets/'),
		filename: 'bundle.js',
		publicPath: '/assets/'
	},
	plugins: [
		new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
				query: {
					presets: ['latest', 'stage-0', 'react']
				}
			},
			{
				test: /\.css$/,
				use: [
					{
						loader: 'style-loader',
						options: {
							sourceMap: true,
							hmr: false
						}
					},
					{
						loader: 'css-loader',
						options: {
							sourceMap: true,
							minimize: true
						}
					}
				]
			},
			{
				test: /\.(png|jpg|gif)$/,
				use: ['file-loader']
			}
		]
	}
}
