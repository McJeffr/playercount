// --------------------------------------------------------
// Modules ------------------------------------------------
// --------------------------------------------------------
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
const merge = require('webpack-merge');

const defaultConfig = require('./webpack.default.js');

// --------------------------------------------------------
// Webpack configuration ----------------------------------
// --------------------------------------------------------
module.exports = merge(defaultConfig, {
	mode: 'production',
	plugins: [
		new UglifyJSPlugin({
			sourceMap: true
		})
	]
});
