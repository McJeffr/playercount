module.exports = {
  port: 8080,
  database: {
    options: {
      // Do NOT include the 'database' option, this goes in the schema configuration instead!
      user: "root",
      password: "",
      timezone: "utc",
      // For more options, see https://github.com/mysqljs/mysql#connection-options
    },
    schema: "playercount",
  },
  endpoint: "https://shotbow.net/serverList.json",
  updateInterval: 15 * 1000, // 15 seconds
  keepData: -1, // indefinitely
  maxDataPoints: 250,
  games: {
    keys: [
      {
        all: "Network totals",
      },
      {
        lobby: "Lobby",
      },
      {
        minez: "MineZ",
      },
      {
        smash: "SMASH",
      },
      {
        ghostcraft: "Ghostcraft",
      },
      {
        hcfactions: "HCFactions",
      },
      {
        annihilation: "Annihilation",
      },
      {
        lms: "Last Man Standing",
      },
      {
        wasted_sandbox: "Wasted",
      },
      {
        wasted_gungame: "GunGame",
      },
      {
        dbv: "Death By Void",
      },
      {
        craftybomber: "CraftyBomber",
      },
      {
        vadact: "Qubion Creative",
      },
      {
        wir: "When in Rogue",
      },
      {
        minez2: "MineZ 2",
      },
      {
        slaughter: "Slaughter",
      },
      {
        uhc: "UHC",
      },
      {
        lightbikes: "Lightbikes",
      },
      {
        hips: "Hidden in Plain Sight",
      },
      {
        mama: "Cooking Mama",
      },
      {
        sweepers: "Sweepers",
      },
      {
        gg: "GG",
      },
      {
        flappy: "Flappy Chick",
      },
      {
        assault: "Assault",
      },
      {
        goldrush: "Gold Rush",
      },
      {
        mta: "Mine Theft Auto",
      },
      {
        civwar: "Civ-War",
      },
      {
        minerproblems: "Miner Problems",
      },
      {
        warband: "Warband",
      },
    ],
    defaultDisplay: [
      "all",
      "minez",
      "smash",
      "annihilation",
      "dbv",
      "slaughter",
      "lobby",
      "gg",
      "wir",
      "minerproblems",
      "warband",
    ],
    total: "all",
  },
};
