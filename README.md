# Playercount

This repository contains a React application together with an API on the server-side. The aim of this application is to provide insight into the playercount of a Minecraft server, in particular that of Shotbow (though it can easily be reconfigured for other servers provided they have an endpoint to fetch playercounts from).

This application makes use of the following technologies.

__Server-side__

- Express
- MySQL
- Winston

__Client-side__

- React 16
- Webpack 4 (with HMR for development)

There is an integration with Docker available. For this, a MySQL server needs to run first. This server can run in a Docker container as well. If there is a MySQL container running, this repository can be built into a Docker image using `docker build -t playercount .` (with the terminal present inside the root directory of this project). After the image has been built, it can be ran using the following command: `docker run -d -p 8080:8080 --link mysql:mysql --name playercount playercount`.

## API Documentation
The API has several endpoints with different query parameters that can be used to customize the output of the API.

`/api/v1/config`: The config endpoint of the API, used to fetch certain configuration options of the server such as the update interval or maximum data points.
`/api/v1/playercount`: The main endpoint of the API to fetch playercounts. There are a few query parameters that can be used to customize what data is sent back:

- `current`: A (boolean) query parameter that, when specified, returns the current live playercount. Can be combined with `after` and `games`, but not with the `top` query parameter.
- `top`: A (boolean) query parameter that, when specified, returns the top of each gamemode. Can be combined with `after` and `games`, but not with the `current` query parameter.
- `after={unix timestamp}`: A query parameter that takes in an unix timestamp (seconds passed since 01/01/1970) to display only the playercount after that timestamp.
- `games={list of games}`: A query parameter that takes in one or multiple games (seperated by commas) that contain the games of which data needs to be sent back.
- `type={type}`: A query parameter that takes in the type of data to be sent back. There are three choices: avg, min or max. The avg type is the default type, though if needed the min or max types can be sent back. When the API fragments the data to be sent according to the maxDataPoints configuration option, it can choose this type to apply how the data in the fragments are processed.

## Disclaimer

This repository is still a work-in-progress and is nowhere near to being done. It is currently not functional and has not been properly tested. Crashes in the server-side have not yet been fully handled, which might result in an unexpected termination of the application. The production environment as configured in Webpack and Docker also is not too efficient yet.

Next to this technical disclaimer, it also has to be pointed out that this repository is not an official Shotbow repository. The data shown in the application is fetched from a [public endpoint](https://shotbow.net/serverList.json) provided by Shotbow. The data fetched from this endpoint can have differences from the actual private data in the hands of Shotbow.