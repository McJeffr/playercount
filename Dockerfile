# Use the latest stable release of NodeJS
FROM node:carbon

# Create a working directory to store all the code in
WORKDIR /usr/src/app

# Copy the package.json file to the working directory and install dependencies
COPY package*.json ./
RUN npm install

# Copy the configs and source code to the working directory
COPY ./config ./config
COPY ./server ./server
COPY ./app ./app

# Expose the port that the app runs on
EXPOSE 8080

# Build the app and clean up all dev dependencies afterwards
RUN npm run build
RUN npm prune --production

# Run the app
CMD ["npm", "run", "prod"]
